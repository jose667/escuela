package com.everis.escuela.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.escuela.dto.ProductDto;
import com.everis.escuela.dto.ProductSaveRequestDto;
import com.everis.escuela.dto.ProductSaveResponseDto;
import com.everis.escuela.mapper.ProductMapper;
import com.everis.escuela.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("product/list")
	public List<ProductDto> list() {
		return ProductMapper.INSTANCE.map(productService.listAll());
	}

	@GetMapping("product/{id}")
	public ProductDto findById(@PathVariable("id") Long id) {
		return ProductMapper.INSTANCE.map(productService.findById(id));
	}

	@PostMapping("product/save")
	public ProductSaveResponseDto save(@RequestBody ProductSaveRequestDto productSaveRequestDto) {
		return ProductMapper.INSTANCE
				.toProductSaveResponseDto(productService.save(ProductMapper.INSTANCE.toEntity(productSaveRequestDto)));
	}
}
