package com.everis.escuela.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.escuela.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
