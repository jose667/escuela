package com.everis.escuela.mapper;

import java.util.List;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.everis.escuela.dto.ProductDto;
import com.everis.escuela.dto.ProductSaveRequestDto;
import com.everis.escuela.dto.ProductSaveResponseDto;
import com.everis.escuela.entity.Product;

@Mapper
public interface ProductMapper {

	ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

	public ProductDto map(Product product);
	
	public List<ProductDto> map (List<Product> products);
	
	public Product toEntity(ProductSaveRequestDto productSaveRequestDto);

	public ProductSaveResponseDto toProductSaveResponseDto(Product product);

	@AfterMapping
	default void setRemainingValues(Product product, @MappingTarget ProductDto productoDto) {
		productoDto.setStatus(Boolean.TRUE.equals(product.getActive()) ? "Active" : "Inactive");
	}
}
